# Setting Up

# Comandos Inicialização

sudo-apt install python3-venv

python3.7 -m venv <nome do diretório>

. <nome do diretório>/bin/activate

pip install flask

deactivate

# Comandos Flask-Migrate

python manage.py db init
python manage.py db migrate
python manage.py db upgrade

#Comandos Mysql

sudo apt-get install libmysqlclient-dev
pip install flask-mysqldb
